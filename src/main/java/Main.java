import java.util.Arrays;
import java.util.List;
import java.util.Scanner;
import number.Fibonacci;
import number.Sorter;
import number.UserNumber;

public class Main {

  public static void main(String[] args) {

    Scanner scanner = new Scanner(System.in);
    System.out.print("Enter minimum number of interval:");
    int minimum = Integer.parseInt(scanner.nextLine());
    System.out.print("Enter maximum number of interval:");
    int maximum = Integer.parseInt(scanner.nextLine());

    List<Integer> numberInterval = new UserNumber().inputNumberInterval(minimum, maximum);

    System.out.println("\n" + Arrays.toString(Sorter.sortEvenOdd(numberInterval)));
    UserNumber.sumOddEven(numberInterval);

    System.out.println(" ");
    System.out.print("Please enter the size of Fibonacci set:");
    int set = Integer.parseInt(scanner.nextLine());

    List<Integer> fibonacci = Fibonacci.
        printFibonacci(UserNumber.findMaxOddandEven(numberInterval),set);

    System.out.println("Fibonacci numbers is : " + fibonacci);

    Fibonacci.pritnPercentageOddandEven(fibonacci);
    scanner.close();
  }
}

