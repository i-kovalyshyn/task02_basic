package number;

import java.util.ArrayList;
import java.util.List;

public class UserNumber {

  public List<Integer> inputNumberInterval(int min, int max) {
    List<Integer> list = new ArrayList<>();
    for (int i = min; i <= max; i++) {
      list.add(i);
    }
    return list;
  }

  public static void sumOddEven(List<Integer> list) {
    int sumOdd = 0;
    int sumEven = 0;
    for (int i : list) {
      if (i % 2 == 0) {
        sumEven += i;
      } else {
        sumOdd += i;
      }
    }
    System.out.println("sum of odd numbers =" + sumOdd + " & sum of even number =" + sumEven);

  }

  public static int[] findMaxOddandEven(List<Integer> list) {

    int biggestEven = list.stream().
        filter((i) -> i % 2 == 0).max(Integer::compareTo).
        orElse(Integer.MAX_VALUE);

    int biggestOdd = list.stream().
        filter((i) -> i % 2 != 0).
        max(Integer::compareTo).
        orElse(Integer.MAX_VALUE);

    return new int[]{biggestOdd, biggestEven};
  }

}
