package number;

import java.util.List;

public class Sorter {

  public static int[] sortEvenOdd(List<Integer> arrList) {
    int even = 0, odd = arrList.size();
    int[] ret = new int[arrList.size()];

    for (int num : arrList) {
      ret[num % 2 == 0 ? --odd : even++] = num;
    }
    return ret;
  }


}
