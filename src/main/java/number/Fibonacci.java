package number;

import java.util.ArrayList;
import java.util.List;

public class Fibonacci {

  public static List<Integer> printFibonacci(int[] maxOddEven, int set) {

    int f3;
    int biggestOdd = maxOddEven[0];
    int biggestEven = maxOddEven[1];

    List<Integer> fibonacci = new ArrayList<>();

    fibonacci.add(biggestOdd);
    fibonacci.add(biggestEven);
    int f1 = biggestEven;
    int f2 = biggestOdd;

    for (int i = 2; i < set; ++i) {

      f3 = f1 + f2;
      fibonacci.add(f3);
      f1 = f2;
      f2 = f3;

    }
    return fibonacci;
  }

  public static void pritnPercentageOddandEven(List<Integer> list) {
    long countEven = list.stream().filter((i) -> i % 2 == 0).count();
    long countOdd = list.stream().filter((i) -> i % 2 != 0).count();

    double percentEven = countEven *100/ list.size();
    double percentOdd = countOdd *100/ list.size();
    System.out.println("Fibonacci is percentage of Odd =" + percentOdd + "%" +
        " & percentage of Even =" + percentEven + "%");

  }
}

